import http from 'http';

const config = Object.freeze({
  port: 8000,
  hostname: '0.0.0.0',
});

const answers = [
  { answer: 'sexy', author: { name:'Veronica', picture:'https://i.pinimg.com/originals/cd/90/d9/cd90d9de63fa2c8e5c5e7117e27b5c18.jpg' } },
  { answer: 'cat', author: { name:'Max', picture:'https://www.indiewire.com/wp-content/uploads/2019/12/avatar-2.jpg' } },
  { answer: 'creative', author: { name:'Oleg', picture:'https://i.pinimg.com/236x/69/c7/bd/69c7bd1fa366f3e6d09596b020b46188.jpg' } },
  { answer: 'dog', author: { name:'Vadim', picture:'https://www.indiewire.com/wp-content/uploads/2019/12/avatar-2.jpg' } },
  { answer: 'animal', author: { name:'Olga', picture:'https://i.pinimg.com/originals/70/0b/30/700b3072751c41e18d1a567ee7ac1c9e.jpg' } },
  { answer: 'burger', author: { name:'Masha', picture:'https://i.pinimg.com/originals/1f/b3/76/1fb3764fb0a25c84d97c707b5585ee6d.jpg' } },
  { answer: 'forest', author: { name:'Dimitri', picture:'https://www.indiewire.com/wp-content/uploads/2019/12/avatar-2.jpg' } },
  { answer: 'airplane', author: { name:'John', picture:'https://i.pinimg.com/originals/05/b1/78/05b1783cb521a13314bffbd7a6495691.jpg' } },
  { answer: 'sea', author: { name:'Dina', picture:'https://i.pinimg.com/originals/fc/03/42/fc03426a5fac006d576da53970a21403.jpg' } },
  { answer: 'ocean', author: { name:'Alice', picture:'https://cdn2.stylecraze.com/wp-content/uploads/2015/03/Layered-Bob-1.jpg' } },
  { answer: 'government', author: { name:'Alex', picture:'https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg' } },
  { answer: 'substitution', author: { name:'Andrew', picture:'https://i.pinimg.com/originals/10/52/96/105296c263190fd06e51ef70ed39b044.jpg' } },
];

const server = http.createServer((request, response) => {
  response.setHeader('Access-Control-Allow-Origin', '*');
  response.setHeader('Content-Type', 'application/json');

  if (request.url === '/api/v1/answers') {
    const shuffledAnswers = answers.sort(() => 0.5 - Math.random());
    response.write(JSON.stringify(shuffledAnswers.slice(0, 3)));
  }

  response.end();
});
server.listen(config.port, config.hostname, () => {
  console.log(`Server is running on http://${config.hostname}:${config.port}`);
});
