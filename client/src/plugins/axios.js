import Vue from 'vue';
import store from '../store';
import axios from 'axios';

const config = Object.freeze({
  baseURL: process.env.baseURL || process.env.apiUrl,
});

const _axios = axios.create(config);

Plugin.install = function(Vue) {
  Object.defineProperties(Vue.prototype, {
    $axios: {
      get() {
        return _axios;
      }
    },
  });
  store.$axios = _axios;
};

Vue.use(Plugin);

export default Plugin;
