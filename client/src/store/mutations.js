const getDefaultState = () => ({
  words: null,
  wordsColors: null,
  puzzle: null,
  selectedLetters: [],
  answers: [],
});

export default {
  SET_WORDS(state, payload) {
    state.words = payload.data;
  },
  SET_WORDS_COLORS(state, payload) {
    state.wordsColors = payload.data;
  },
  SET_PUZZLE(state, payload) {
    state.puzzle = payload.data;
  },
  ADD_SELECTED_LETTERS(state, payload) {
    state.selectedLetters.push(...payload.data);
  },
  ADD_ANSWERS(state, payload) {
    state.answers.push(...payload.data);
  },
  clearState(state) {
    Object.assign(state, getDefaultState());
  },
}