export default {
  getWords({ dispatch, commit }) {
    const words = ['sexy', 'forest', 'ocean', 'creative', 'airplane', 'motivation', 'apartment', 'government', 'substitution', 'burger'];
    commit('SET_WORDS', { data: words });
    dispatch('generateWordsColors');
  },
  generateWordsColors({ commit, state }) {
    const colors = {};
    state.words.forEach(word => colors[word] = '#' + Math.floor(Math.random() * 16777215).toString(16));
    commit('SET_WORDS_COLORS', { data: colors });
  },
  generatePuzzle({ commit, state }) {
    const puzzle = window.wordfind.newPuzzle(state.words);
    commit('SET_PUZZLE', { data: puzzle });
  },
  async getAnswers({ commit, state }) {
    const { data } = await this.$axios.get('/api/v1/answers');
    const solution = window.wordfind.solve(state.puzzle, data.map(answer => answer.answer));
    const correctAnswers = data.filter(answer => !solution.notFound.includes(answer.answer));
    const letters = [];

    solution.found
        .filter(solution => !state.answers.some(answer => answer.answer === solution.word))
        .forEach(solution => {
          const next = window.wordfind.orientations[solution.orientation];
          for (let i = 0; i < solution.word.length; i++) {
            const coordinates = next(solution.x, solution.y, i);
            letters.push({ coordinates: [coordinates.y, coordinates.x], color: state.wordsColors[solution.word] });
          }
    });

    commit('ADD_SELECTED_LETTERS', { data: letters });
    commit('ADD_ANSWERS', { data: correctAnswers });
  },
}