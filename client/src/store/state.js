export default {
  words: null,
  wordsColors: null,
  puzzle: null,
  selectedLetters: [],
  answers: [],
}
